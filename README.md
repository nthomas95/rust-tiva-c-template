This repository is meant to be used as a template for starting projects on the [Tiva C development board](https://www.ti.com/tool/EK-TM4C123GXL).

The tiva C board has a [TM4C123GH6PM](https://www.ti.com/product/TM4C123GH6PM) ARM® [Cortex-M4F](https://developer.arm.com/Processors/Cortex-M4) based microcontroller. 
It has 256kB of flash and 32 kB of static random access memory.

https://developer.arm.com/Processors/Cortex-M4
https://doc.rust-lang.org/nightly/rustc/platform-support/thumbv7em-none-eabi.html
https://doc.rust-lang.org/stable/rustc/platform-support.html

## Setup

~~~shell
rustup target add thumbv7em-none-eabihf
~~~

Verify that the target is installed correctly.

~~~shell
rustup show
~~~

The rust compiler needs to run the linker script and compile for the target. This can be done with 

~~~shell
cargo rustc --target thumbv7m-none-eabi -- \
      -C link-arg=-nostartfiles -C link-arg=-Tlink.x
~~~

Cargo is configured to automatically use these arguments in the `.cargo/config.toml` file.

install probe-rs (cargo-embed) and cargo-binutils

~~~shell
probe-rs chip list
~~~

Cortex-M4


~~~shell
cat /etc/udev/rules.d/99-stellaris-launchpad.rules
ATTRS{idVendor}=="1cbe", ATTRS{idProduct}=="00fd", GROUP="users", MODE="0660"
~~~
