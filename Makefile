.PHONY: flash
flash: 
	cargo build
	arm-none-eabi-objcopy -O binary target/thumbv7em-none-eabihf/debug/rust-tiva-c-template target/thumbv7em-none-eabihf/debug/rust-tiva-c-template.bin
	lm4flash target/thumbv7em-none-eabihf/debug/rust-tiva-c-template.bin
