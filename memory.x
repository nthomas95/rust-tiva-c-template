/*
    Linker script for the TM4C123GH6PM 
    Flash starts at 0x0000.0000 and SRAM starts at  0x2000.0000
*/
MEMORY
{
  FLASH : ORIGIN = 0x00000000, LENGTH = 256K
  RAM : ORIGIN = 0x20000000, LENGTH = 32K
}
