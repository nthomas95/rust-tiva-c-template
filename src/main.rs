#![no_std]
#![no_main]

use core::ptr::write_volatile;

use cortex_m::asm::nop;     // Low level access to Cortex-M processors
use cortex_m_rt::entry;     // Startup and minimal runtime code
use panic_halt as _;        // Provides bare minimum panic handling (shouldn't be used in
                            // production environment).

fn delay(cycles: u32) {
    for _ in 0..cycles {
        nop();
    }
}

#[entry]
fn main() -> ! {
    const GPIO_PORT_F_START_ADDR: u32 = 0x4002_5000;    // Starting address of the GPIO Port F
                                                        // memory block
    const SYSTEM_CONTROL_START_ADDR: u32 = 0x400F_E000; // Starting address of the system control
                                                        // memory block

    // Puts a 1 in the fifth bit of the run mode clock gating register, which will enable the
    // clock signal to GPIO Port F.
    const GPIO_RUN_MODE_CLOCK_GATING_REGISTER: u32 = SYSTEM_CONTROL_START_ADDR + 0x608;
    const ENABLE_PORT_F: u32 = 1 << 5;
    unsafe {
        write_volatile(GPIO_RUN_MODE_CLOCK_GATING_REGISTER as *mut u32, ENABLE_PORT_F);
    }

    // Constants for the RGB LED positions in the GPIO Port F data and digital enable registers
    const LED_RED: u32 = 0b0010;
    const LED_BLUE: u32 = 0b0100;
    const LED_GREEN: u32 = 0b1000;
    const RGB_LEDS: u32 = LED_RED | LED_GREEN | LED_BLUE;

    // Enable the LED bits (digital enable).
    const GPIO_PORT_F_DEN_REG_ADDR: u32 = GPIO_PORT_F_START_ADDR + 0x51C;
    unsafe {
        write_volatile(GPIO_PORT_F_DEN_REG_ADDR as *mut u32, RGB_LEDS);
    }

    // Set direction LED bits to output.
    const GPIO_PORT_F_DIR_REG_ADDR: u32 = GPIO_PORT_F_START_ADDR + 0x400;
    unsafe {
        write_volatile(GPIO_PORT_F_DIR_REG_ADDR as *mut u32, RGB_LEDS);
    }

    // In order to write to GPIODATA, the corresponding bits in the mask, resulting from the address bus 
    // bits [9:2], must be set. Otherwise, the bit values remain unchanged by the write. This value
    // sets all bits in the address bus to 1.
    const GPIO_ALL_BITS_MASK: u32 = 0x3FC;

    loop {
        // Enable the LEDs
        unsafe {
            write_volatile((GPIO_PORT_F_START_ADDR + GPIO_ALL_BITS_MASK) as *mut u32, RGB_LEDS);
        }
        delay(100_000);
        // Disable the LEDs
        unsafe {
            write_volatile((GPIO_PORT_F_START_ADDR + GPIO_ALL_BITS_MASK) as *mut u32, 0);
        }
        delay(100_000);
    }
}
